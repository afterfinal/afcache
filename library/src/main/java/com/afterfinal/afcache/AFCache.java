package com.afterfinal.afcache;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.io.File;
import java.lang.reflect.Method;

@SuppressWarnings("all")
public class AFCache {
    private static File sCacheDir;
    private static final Handler handler = new Handler(Looper.getMainLooper());
    private static CacheFilter sCacheFilter;
    private static KeyDecryptor sKeyDecryptor;

    private CacheServer server;

    private static Context sContext = null;

    public static void init(Context context) {
        sContext = context.getApplicationContext();
    }

    public static KeyDecryptor getKeyUriDecryptor() {
        return sKeyDecryptor;
    }

    public static void setKeyUriDecryptor(KeyDecryptor decryptor) {
        AFCache.sKeyDecryptor = decryptor;
    }

    public void getUrl(String originUrl, CacheServer.Callback callback) {
        if (null == server) {
            server = new CacheServer();
        }
        CacheServerFactory.getUrl(originUrl, callback);
    }

    public static void setCacheDir(File dir) {
        sCacheDir = dir;
    }

    public static File getCacheDir() {
        if (null == sCacheDir) {
            return new File(getContext().getExternalCacheDir(), "afcaches");
        }
        return sCacheDir;
    }

    public static void setCacheFilter(CacheFilter filter) {
        AFCache.sCacheFilter = filter;
    }

    public static CacheFilter getCacheFilter() {
        return sCacheFilter;
    }

    public static Context getContext() {
        if (null != sContext) {
            return sContext;
        }
        Application application = null;
        try {
            Class atClass = Class.forName("android.app.ActivityThread");
            Method currentApplicationMethod = atClass.getDeclaredMethod("currentApplication");
            currentApplicationMethod.setAccessible(true);
            application = (Application) currentApplicationMethod.invoke(null);
        } catch (Exception e) {
        }

        if (application != null)
            return application;

        try {
            Class atClass = Class.forName("android.app.AppGlobals");
            Method currentApplicationMethod = atClass.getDeclaredMethod("getInitialApplication");
            currentApplicationMethod.setAccessible(true);
            application = (Application) currentApplicationMethod.invoke(null);
        } catch (Exception e) {
        }
        sContext = application;
        return application;
    }

    public interface Callback extends CacheServer.Callback {

    }

    public interface KeyDecryptor {

        String decrypt(String method, String uri);
    }
}
