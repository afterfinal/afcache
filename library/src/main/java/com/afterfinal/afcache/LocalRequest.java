package com.afterfinal.afcache;

import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

public class LocalRequest {
    private final CaseInsensitiveMap headers = new CaseInsensitiveMap();

    private final String method;
    private final String path;
    private final String url;
    private final String protocol;

    private String[] range = null;

    public LocalRequest(HttpServletRequest request) throws IOException {
        this.method = request.getMethod();
        this.path = request.getPathInfo();
        this.protocol = request.getProtocol();
        this.url = Util.parseUrl(path);

        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String name = headerNames.nextElement();
            if (null != name && Headers.request().support(name)) {
                String value = request.getHeader(name);
                headers.put(name, value);
                if (Consts.KEY_RANGE.equalsIgnoreCase(name)) {
                    Pattern pattern = Pattern.compile("bytes=(\\d*)-(\\d*)", Pattern.CASE_INSENSITIVE);
                    Matcher matcher = pattern.matcher(value);
                    if (matcher.matches()) {
                        range = new String[]{matcher.group(1), matcher.group(2)};
                    }
                }
            }
        }
    }

    public String getMethod() {
        return method;
    }

    public String getPath() {
        return path;
    }

    public String getUrl() {
        return url;
    }

    public String getProtocol() {
        return protocol;
    }

    public boolean isPartial() {
        return null != range;
    }

    public String[] getRange() {
        return range;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public String getHeader(String name) {
        return headers.get(name);
    }

    @Override
    public String toString() {
        return "Request{" +
                "method='" + method + '\'' +
                ", path='" + path + '\'' +
                ", protocol='" + protocol + '\'' +
                '}';
    }
}
