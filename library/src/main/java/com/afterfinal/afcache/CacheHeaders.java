package com.afterfinal.afcache;

import com.google.gson.reflect.TypeToken;

import java.net.HttpURLConnection;
import java.util.Map;

class CacheHeaders {
    private final CaseInsensitiveMap data = new CaseInsensitiveMap();

    public String get(String key, String fallback) {
        return data.containsKey(key) ? data.get(key) : fallback;
    }

    public String getOrDefault(String key, String fallback) {
        return data.containsKey(key) ? data.get(key) : fallback;
    }

    public void put(String key, String value) {
        if (Headers.response().support(key)) {
            data.put(key, value);
        }
    }

    public void parse(String headers) {
        data.clear();
        data.putAll(GsonUtil.fromJson(headers, new TypeToken<Map<String, String>>() {
        }.getType()));
    }

    public void parse(HttpURLConnection connection) {
        data.clear();
        for (String key : connection.getHeaderFields().keySet()) {
            String value = connection.getHeaderField(key);
            put(key, value);
            if (Consts.KEY_CONTENT_RANGE.equalsIgnoreCase(key)) {
                if (value.contains("/")) {
                    put(Consts.KEY_CONTENT_LENGTH, value.split("/")[1]);
                }
            }
        }
    }

    public long getContentLength() {
        return Long.parseLong(get(Consts.KEY_CONTENT_LENGTH, "0"));
    }

    public String getContentType() {
        return get(Consts.KEY_CONTENT_TYPE, null);
    }

    @Override
    public String toString() {
        return GsonUtil.toJson(this);
    }

    public Map<String, String> map() {
        CaseInsensitiveMap map = new CaseInsensitiveMap();
        map.putAll(this.data);
        return map;
    }
}
