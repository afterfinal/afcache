package com.afterfinal.afcache;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.afterfinal.afcache.db.Column;
import com.afterfinal.afcache.db.DbHelper;

import java.io.File;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Cache {
    private static final ExecutorService sExecutor = Executors.newCachedThreadPool();
    private static final String TB_NAME = Cache.class.getSimpleName();

    private static final Object lock = new Object();

    @Column(order = 1, primaryKey = true, notNull = true)
    private String url;
    @Column(order = 2)
    private String parentUrl;
    @Column(order = 3)
    private String filePath;
    @Column(order = 4)
    private String fileName;
    @Column(order = 5)
    private long myCurrent;
    @Column(order = 6)
    private long myTotal;
    @Column(order = 7)
    private long createTime;

    public Cache() {
        //no instance
    }

    public static Cache get(String url) throws Exception {
        Cache cache = null;
        synchronized (lock) {
            try (Cursor cursor = DbHelper.getInstance().getReadableDatabase().rawQuery("select * from " + TB_NAME + " where url='" + url + "'", null)) {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    cache = new Cache();
                    cache.readEntity(cursor);
                }
            }
            if (null == cache) {
                cache = new Cache();
                cache.setUrl(url);
                cache.setFilePath(Util.getCacheFilePath(url));
                cache.setCreateTime(System.currentTimeMillis());

                DbHelper.getInstance().getWritableDatabase().execSQL(String.format(Locale.getDefault(), "INSERT INTO %s(url,filePath,createTime,sequence) VALUES ('%s', '%s', %d, %d)", TB_NAME, cache.getUrl(), cache.getFilePath(), cache.getCreateTime(), cache.getCreateTime()));
            }
        }
        return cache;
    }

    private volatile boolean isStart = false;
    private volatile HttpURLConnection connection = null;

    public static void createTable(SQLiteDatabase db) {
        StringBuilder sql = new StringBuilder("CREATE TABLE ").append(TB_NAME).append("(");
        List<Field> fields = Util.getColumns(Cache.class);
        boolean first = true;
        for (Field field : fields) {
            Column column = field.getAnnotation(Column.class);
            field.setAccessible(true);
            if (!first) {
                sql.append(",");
            } else {
                first = false;
            }
            boolean isNumeric = false;
            if (String.class == field.getType()) {
                sql.append(field.getName()).append(" TEXT");
            } else if (Byte.class == field.getType() || byte.class == field.getType()) {
                sql.append(field.getName()).append(" INTEGER");
            } else if (Long.class == field.getType() || long.class == field.getType()) {
                isNumeric = true;
                sql.append(field.getName()).append(" INTEGER");
            } else if (Integer.class == field.getType() || int.class == field.getType()) {
                isNumeric = true;
                sql.append(field.getName()).append(" INTEGER");
            } else if (Float.class == field.getType() || float.class == field.getType()) {
                sql.append(field.getName()).append(" REAL");
            } else if (Double.class == field.getType() || double.class == field.getType()) {
                sql.append(field.getName()).append(" REAL");
            } else if (Boolean.class == field.getType() || boolean.class == field.getType()) {
                sql.append(field.getName()).append(" INTEGER");
            } else if (Byte[].class == field.getType() || byte[].class == field.getType()) {
                sql.append(field.getName()).append(" BLOB");
            }
            if (column.primaryKey()) {
                sql.append(" PRIMARY KEY");
            }
            if (column.notNull()) {
                sql.append(" NOT NULL");
            }
            if (column.unique()) {
                sql.append(" UNIQUE");
            }
            if (isNumeric && column.autoIncrement()) {
                sql.append(" AUTOINCREMENT");
            }
        }
        sql.append(")");
        db.execSQL(sql.toString());
    }

    public void start() {
        isStart = true;
        sExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    connection = Util.createConnection(getUrl());
                    byte[] buffer = new byte[1024];
                    while (isStart && connection.getInputStream().read(buffer) > 0) {
                        //TODO nothing
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (null != connection) {
                    try {
                        connection.disconnect();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    connection = null;
                }
            }
        });
    }

    public void stop() {
        isStart = false;
        if (null != connection) {
            try {
                connection.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }
            connection = null;
        }
    }

    public boolean save() throws SQLiteException {
        synchronized (lock) {
            try {
                List<Field> fields = Util.getColumns(Cache.class);
                ContentValues values = new ContentValues();
                for (Field field : fields) {
                    field.setAccessible(true);
                    String name = field.getName();
                    Object value = field.get(this);
                    if (CharSequence.class.isAssignableFrom(field.getType())) {
                        values.put(name, null == value ? null : value.toString());
                    } else if (Boolean.class == field.getType() || boolean.class == field.getType()) {
                        values.put(name, null == value ? null : (Boolean) value);
                    } else if (Byte.class == field.getType() || byte.class == field.getType()) {
                        values.put(name, (Byte) value);
                    } else if (Short.class == field.getType() || short.class == field.getType()) {
                        values.put(name, (Short) value);
                    } else if (Integer.class == field.getType() || int.class == field.getType()) {
                        values.put(name, (Integer) value);
                    } else if (Long.class == field.getType() || long.class == field.getType()) {
                        values.put(name, (Long) value);
                    } else if (Float.class == field.getType() || float.class == field.getType()) {
                        values.put(name, (Float) value);
                    } else if (Double.class == field.getType() || double.class == field.getType()) {
                        values.put(name, (Double) value);
                    } else if (Byte[].class == field.getType() || byte[].class == field.getType()) {
                        values.put(name, (byte[]) value);
                    }
                }
                try (Cursor cursor = DbHelper.getInstance().getReadableDatabase().rawQuery("select url from " + TB_NAME + " where url='" + getUrl() + "'", null)) {
                    int count = cursor.getCount();
                    if (0 == count) {
                        DbHelper.getInstance().getWritableDatabase().insert(TB_NAME, null, values);
                    } else {
                        DbHelper.getInstance().getWritableDatabase().update(TB_NAME, values, "url='%s'", new String[]{getUrl()});
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public boolean delete() throws SQLiteException {
        synchronized (lock) {
            try {
                try (Cursor cursor = DbHelper.getInstance().getWritableDatabase().rawQuery("select * from " + TB_NAME + " where url='" + url + "' or parentUrl='" + url + "'", null)) {
                    if (cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        do {
                            String filePath = cursor.getString(cursor.getColumnIndex("filePath"));
                            if (null != filePath) {
                                deleteFiles(new File(filePath));
                            }
                        } while (cursor.moveToNext());
                    }
                }
                DbHelper.getInstance().getWritableDatabase().execSQL("delete from " + TB_NAME + " where url='" + url + "' or parentUrl='" + url + "'", null);
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private void deleteFiles(File file) {
        if (file.isDirectory()) {
            File[] subFiles = file.listFiles();
            if (null != subFiles) {
                for (File subFile : subFiles) {
                    deleteFiles(subFile);
                }
            }
        }
        file.delete();
    }

    public void load() throws Exception {
        try (Cursor cursor = DbHelper.getInstance().getReadableDatabase().rawQuery("select * from " + TB_NAME + " where url='" + url + "'", null)) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                readEntity(cursor);
            }
        }
    }

    private void readEntity(Cursor cursor) throws Exception {
        List<Field> fields = Util.getColumns(Cache.class);
        for (Field field : fields) {
            Column column = field.getAnnotation(Column.class);
            if (null != column) {
                field.setAccessible(true);
                if (String.class == field.getType()) {
                    field.set(this, DbHelper.getString(cursor, field.getName()));
                } else if (Long.class == field.getType() || long.class == field.getType()) {
                    field.set(this, DbHelper.getInt(cursor, field.getName()));
                } else if (Integer.class == field.getType() || int.class == field.getType()) {
                    field.set(this, DbHelper.getInt(cursor, field.getName()));
                } else if (Float.class == field.getType() || float.class == field.getType()) {
                    field.set(this, DbHelper.getFloat(cursor, field.getName()));
                } else if (Double.class == field.getType() || double.class == field.getType()) {
                    field.set(this, DbHelper.getDouble(cursor, field.getName()));
                }
            }
        }
    }

    public long getCurrent() {
        synchronized (lock) {
            StringBuilder sql = new StringBuilder("select sum(myCurrent) as current from ").append(TB_NAME);
            sql.append(" where url='").append(getUrl()).append("'");
            if (null != getParentUrl()) {
                sql.append(" or parentUrl='").append(getUrl()).append("'");
            }
            try (Cursor cursor = DbHelper.getInstance().getReadableDatabase().rawQuery(sql.toString(), null)) {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    return cursor.getLong(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return -1;
    }

    public long getTotal() {
        synchronized (lock) {
            StringBuilder sql = new StringBuilder("select sum(myTotal) as total from ").append(TB_NAME);
            sql.append(" where url='").append(getUrl()).append("'");
            if (null != getParentUrl()) {
                sql.append(" or parentUrl='''").append(getUrl()).append("'");
            }
            try (Cursor cursor = DbHelper.getInstance().getReadableDatabase().rawQuery(sql.toString(), null)) {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    return cursor.getLong(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return -1;
    }

    public static Cache nextUnfinishedCache() {
        synchronized (lock) {
            String sql = "select * from " + TB_NAME + " where myCurrent < myTotal order by sequence";
            try (Cursor cursor = DbHelper.getInstance().getReadableDatabase().rawQuery(sql, null)) {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    Cache cache = new Cache();
                    for (int i = 0; i < cursor.getColumnCount(); i++) {
                        String name = cursor.getColumnName(i);
                        Field field = Cache.class.getDeclaredField(name);
                        field.setAccessible(true);
                        field.set(cache, parseValue(field.getType(), cursor, i));
                    }
                    cache.setUrl(cursor.getString(0));
                    cache.setParentUrl(cursor.getString(1));
                    cache.setFilePath(cursor.getString(2));
                    cache.setFileName(cursor.getString(3));
                    cache.setMyCurrent(cursor.getLong(4));
                    cache.setMyTotal(cursor.getLong(5));
                    cache.setCreateTime(cursor.getLong(6));
                    return cache;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private static Object parseValue(Class<?> type, Cursor cursor, int columnIndex) {
        if (type == Short.class || type == short.class) {
            return cursor.getShort(columnIndex);
        }
        if (type == Integer.class || type == int.class) {
            return cursor.getInt(columnIndex);
        }
        if (type == Long.class || type == long.class) {
            return cursor.getLong(columnIndex);
        }
        if (type == Float.class || type == float.class) {
            return cursor.getFloat(columnIndex);
        }
        if (type == Double.class || type == double.class) {
            return cursor.getDouble(columnIndex);
        }
        if (type == Boolean.class || type == boolean.class) {
            return 1 == cursor.getInt(columnIndex);
        }
        if (CharSequence.class.isAssignableFrom(type)) {
            return cursor.getString(columnIndex);
        }
        if (type == byte[].class) {
            return cursor.getBlob(columnIndex);
        }
        return null;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getParentUrl() {
        return parentUrl;
    }

    public void setParentUrl(String parentUrl) {
        this.parentUrl = parentUrl;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getMyCurrent() {
        return myCurrent;
    }

    public void setMyCurrent(long myCurrent) {
        this.myCurrent = myCurrent;
    }

    public long getMyTotal() {
        return myTotal;
    }

    public void setMyTotal(long myTotal) {
        this.myTotal = myTotal;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }
}
