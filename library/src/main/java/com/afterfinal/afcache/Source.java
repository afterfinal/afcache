package com.afterfinal.afcache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class Source {
    private final String url;

    private final CacheHeaders cacheHeaders = new CacheHeaders();

    private final List<Source> subCaches = new ArrayList<>();

    private LocalRequest request = null;

    private final AtomicBoolean isOpen = new AtomicBoolean(false);

    private Cache cache;

    private File contentCacheFile;

    public Source(String url) {
        this.url = url;
    }

    public Source(LocalRequest request) {
        this.request = request;
        this.url = request.getUrl();
    }

    public boolean isOpen() {
        return isOpen.get();
    }

    public synchronized void open() throws Exception {
        Logger.d("source.open:" + url);

        cache = Cache.get(url);
        contentCacheFile = new File(cache.getFilePath());

        if ("1".equals(request.getHeader(Consts.KEY_CLEAR_CACHE))) {
            clearCache();
        }

        ensureFileExists();

        parseHeaders();
        checkRemoteChange();

        ensureFileExists();

        cache.setMyTotal(cacheHeaders.getContentLength());
        if (isM3u8()) {
            cache.setMyCurrent(cacheHeaders.getContentLength());
        }
        cache.save();

        isOpen.set(true);

        parseSubCaches();

        DownloadManager.enqueue(cache);
    }

    private void ensureFileExists() throws Exception {
        FileLock.withFileLocked(contentCacheFile.getAbsolutePath(), true, new FileLock.Runnable() {
            @Override
            public void run() throws Exception {
                Util.ensureFileExists(contentCacheFile);
            }
        });
    }

    private void parseHeaders() throws Exception {
        FileLock.withFileLocked(cache.getFilePath(), true, new FileLock.Runnable() {
            @Override
            public void run() throws Exception {
                cache.setLocalRequest(GsonUtil.toJson(request));
                if (null == cache.getResponseHeaders()) {
                    HttpURLConnection connection = Util.createHeadConnection(request);
                    cacheHeaders.parse(connection);
                    connection.disconnect();
                    cache.setResponseHeaders(cacheHeaders.toString());

                    if (isM3u8()) {
                        FileLock.withFileLocked(contentCacheFile.getAbsolutePath(), true, new FileLock.Runnable() {
                            @Override
                            public void run() throws Exception {
                                FileOutputStream fos = new FileOutputStream(contentCacheFile);
                                HttpURLConnection contentConnection = Util.createConnection(request);
                                List<String> lines = Util.parseStreamToLines(request, contentConnection.getInputStream());
                                contentConnection.disconnect();
                                for (String line : lines) {
                                    fos.write(((line + "\n").getBytes()));
                                }
                                fos.close();
                                long size = contentCacheFile.length();
                                cacheHeaders.put(Consts.KEY_CONTENT_LENGTH, String.valueOf(size));
                            }
                        });
                    }
                    String disposition = cacheHeaders.get(Consts.KEY_CONTENT_DISPOSITION, null);
                    String fileName;
                    if (null != disposition) {
                        String search = "filename=";
                        fileName = disposition.substring(disposition.toLowerCase().indexOf(search) + search.length());
                        fileName = fileName.trim().replaceAll("\\s*;$", "");
                    } else {
                        fileName = url.substring(url.lastIndexOf("/") + 1);
                    }
                    cache.setFileName(fileName);
                    cache.setResponseHeaders(cacheHeaders.toString());
                    cache.save();
                } else {
                    cacheHeaders.parse(cache.getResponseHeaders());
                }
            }
        });
    }

    private void parseSubCaches() throws Exception {
        if (!isM3u8()) {
            return;
        }

        List<String> urls = new ArrayList<>();
        if (isMeCacheComplete()) {
            FileInputStream is = new FileInputStream(contentCacheFile);
            urls = Util.parseUrlsFromM3u8(url, is);
            is.close();
        } else if (null != request) {
            HttpURLConnection connection = Util.createConnection(request, null);
            urls = Util.parseUrlsFromM3u8(url, connection.getInputStream());
            connection.disconnect();
        }
        for (String url : urls) {
            Cache cache = Cache.get(url);
            cache.setParentUrl(Source.this.url);
            cache.save();
            subCaches.add(new Source(url));
        }
    }

    private boolean isM3u8() throws Exception {
        String contentType = cacheHeaders.getContentType();
        if (null == contentType) {
            HttpURLConnection conn = Util.createHeadConnection(request);
            contentType = conn.getContentType();
        }
        return "application/x-mpegurl|application/vnd.apple.mpegurl".contains(contentType.toLowerCase());
    }

    private boolean isMeCacheComplete() {
        return null != cache && null != cache.getResponseHeaders() && contentCacheFile.exists() && cacheHeaders.getContentLength() > 0 && cacheHeaders.getContentLength() == contentCacheFile.length();
    }

    public boolean isCacheComplete() {
        boolean isCacheComplete = isMeCacheComplete();
        for (Source subCache : subCaches) {
            isCacheComplete &= subCache.isCacheComplete();
        }
        return isCacheComplete;
    }

    public long totalLength() {
        long size = contentCacheFile.length();
        for (Source subCache : subCaches) {
            size += subCache.totalLength();
        }
        return Long.parseLong(cacheHeaders.get(Consts.KEY_CONTENT_LENGTH, "0")) + size;
    }

    public long cacheLength() {
        long size = 0;
        for (Source subCache : subCaches) {
            size += subCache.cacheLength();
        }
        return contentCacheFile.length() + size;
    }

    public void clearCache() throws Exception {
        FileLock.withFileLocked(contentCacheFile.getAbsolutePath(), true, new FileLock.Runnable() {
            @Override
            public void run() throws Exception {
                deleteFile(contentCacheFile);
            }
        });

        for (Source subCache : subCaches) {
            subCache.clearCache();
        }
    }

    private void deleteFile(File file) {
        try {
            if (null != file && file.exists()) {
                if (file.isDirectory()) {
                    File[] subFiles = file.listFiles();
                    if (null != subFiles) {
                        for (File subFile : subFiles) {
                            deleteFile(subFile);
                        }
                    }
                }
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private long getContentLength() {
        return cacheHeaders.getContentLength();
    }

    public String getHeader(String key) {
        return cacheHeaders.get(key, null);
    }

    RandomAccessFile accessFile = null;
    long total = 0;

    public int read(byte[] buffer) throws IOException {
        if (null == accessFile) {
            accessFile = new RandomAccessFile(contentCacheFile, "r");
        }
        if (total >= getContentLength()) {
            return 0;
        }
        while (total >= cacheLength()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                //TODO nothing
            }
        }
        int count = accessFile.read(buffer);
        total += count;
        return count;
    }

    public void close() throws IOException {
        isOpen.set(false);
        if (null != accessFile) {
            accessFile.close();
        }
    }

    public Map<String, String> headers() {
        return cacheHeaders.map();
    }

    private String getContentType() {
        return cacheHeaders.getContentType();
    }

    private void checkSourceOpen() throws IOException {
        if (!isOpen()) {
            throw new IOException("source closed");
        }
    }

    private void checkRemoteChange() {
        try {
            HttpURLConnection conn = Util.createHeadConnection(request);
            String lastModified = conn.getHeaderField(Consts.KEY_LAST_MODIFIED);
            String localLastModified = getHeader(Consts.KEY_LAST_MODIFIED);
            if (null != lastModified && !lastModified.equals(localLastModified)) {
                clearCache();
            }
        } catch (Exception e) {
            //TODO nothing
        }
    }
}
