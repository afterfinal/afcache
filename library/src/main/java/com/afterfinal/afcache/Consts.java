package com.afterfinal.afcache;

public class Consts {
    public static final String KEY_CLEAR_CACHE = "Clear-Cache";

    public static final String AFCacheSchema = "AFCacheSchema";

    public static final String KEY_HOST = "Host";//Host: 172.16.95.123:8080
    public static final String KEY_CONNECTION = "Connection";//Connection: keep-alive
    public static final String KEY_ACCEPT = "Accept";//Accept: */*
    public static final String KEY_REFERER = "Referer";//http://172.16.95.123:8080/video/big_buck_bunny.mp4
    public static final String KEY_RANGE = "Range";//Range: bytes=3670016-5510871
    public static final String KEY_IF_RANGE = "If-Range";//If-Range: W/"5510872-1583977937551"
    public static final String KEY_ACCEPT_ENCODING = "Accept-Encoding";//Accept-Encoding: identity;q=1, *;q=0
    public static final String KEY_ACCEPT_LANGUAGE = "Accept-Language";//Accept-Language: zh-CN,zh;q=0.9

    public static final String KEY_CONTENT_TYPE = "Content-Type";//Content-Type: video/mp4
    public static final String KEY_CONTENT_LENGTH = "Content-Length";//Content-Length: 1840856
    public static final String KEY_CONTENT_RANGE = "Content-Range";//Content-Range: bytes 0-5510871/5510872
    public static final String KEY_ACCEPT_RANGES = "Accept-Ranges";//Accept-Ranges: bytes
    //public static final String KEY_CONNECTION = "Connection";//Connection: keep-alive
    public static final String KEY_KEEP_ALIVE = "Keep-Alive";//Keep-Alive: timeout=60

    public static final String KEY_COOKIE = "Cookie";//Cookie:

    public static final String KEY_SET_COOKIE = "Set-Cookie";
    public static final String KEY_LAST_MODIFIED = "Last-Modified";
    public static final String KEY_AGE = "Age";
    public static final String KEY_X_SWIFT_CACHE_TIME = "X-Swift-CacheTime";
    public static final String KEY_ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
    public static final String KEY_CONTENT_DISPOSITION = "Content-Disposition";
    public static final String KEY_METHOD = "Method";
    public static final String KEY_REMOTE_ADDR = "remote-addr";
    public static final String KEY_HTTP_CLIENT_IP = "http-client-ip";
}







