package com.afterfinal.afcache;

import com.google.gson.reflect.TypeToken;

import java.net.HttpURLConnection;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.GZIPOutputStream;

public class CaseInsensitiveMap extends TreeMap<String, String> {
    public CaseInsensitiveMap() {
        super(String.CASE_INSENSITIVE_ORDER);
    }

    public String get(String key, String fallback) {
        return containsKey(key) ? get(key) : fallback;
    }

    @Override
    public String getOrDefault(Object key, String fallback) {
        return containsKey(key) ? get(key) : fallback;
    }

    @Override
    public String put(String key, String value) {
        if (Headers.response().support(key)) {
            return super.put(key, value);
        }
        return null;
    }

    public void parse(HttpURLConnection connection) {
        clear();
        for (String key : connection.getHeaderFields().keySet()) {
            if (null != key) {
                String value = connection.getHeaderField(key);
                put(key, value);
                if (Consts.KEY_CONTENT_RANGE.equalsIgnoreCase(key)) {
                    if (value.contains("/")) {
                        put(Consts.KEY_CONTENT_LENGTH, value.split("/")[1]);
                    }
                }
            }
        }
    }

    public long getContentLength() {
        return Long.parseLong(get(Consts.KEY_CONTENT_LENGTH, "0"));
    }

    public String getContentType() {
        return get(Consts.KEY_CONTENT_TYPE, null);
    }

    @Override
    public String toString() {
        return GsonUtil.toJson(this);
    }

    public void parse(String headers) {
        Map<String, String> map = GsonUtil.fromJson(headers, new TypeToken<Map<String, String>>() {
        }.getType());
        clear();
        putAll(map);
    }
}
