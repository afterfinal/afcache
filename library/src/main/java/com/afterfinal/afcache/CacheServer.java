package com.afterfinal.afcache;

import android.os.Handler;
import android.os.Looper;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.util.component.LifeCycle;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CacheServer {
    private static final int MAX_QUEUE_SIZE = Integer.MAX_VALUE;
    protected int localPort = 0;

    protected static final String HOST_NAME = "127.0.0.1";

    protected static final String PING_URL = "ping";
    protected static final int PING_RESPONSE_CODE = 204;

    protected static final Handler handler = new Handler(Looper.getMainLooper());
    protected static final ExecutorService sExecutor = Executors.newCachedThreadPool();

    private volatile State state = State.Stopped;

    private StateCallback stateCallback;
    private final AtomicInteger connections = new AtomicInteger();

    public State getState() {
        return state;
    }

    public void setState(State state) {
        State s = this.state;
        this.state = state;
        if (null != stateCallback) {
            stateCallback.onStateChanged(s, state);
        }
    }

    public boolean isAlive() {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(makeUrl(PING_URL)).openConnection();
            return PING_RESPONSE_CODE == connection.getResponseCode();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * ****************************************************************************************
     **/

    private Server jettyServer;

    public CacheServer() {
        this(0);
    }

    public CacheServer(int port) {
        this.localPort = port;
    }

    public void start() {
        setState(State.Starting);
        sExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (null != jettyServer) {
                        try {
                            jettyServer.stop();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        jettyServer = null;
                    }
                    try {
                        ServerSocket serverSocket = new ServerSocket(localPort);
                        localPort = serverSocket.getLocalPort();
                        serverSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    jettyServer = new Server();
                    SelectChannelConnector selectChannelConnector = new SelectChannelConnector();
                    selectChannelConnector.setUseDirectBuffers(false);
                    selectChannelConnector.setPort(localPort);
                    selectChannelConnector.setHost(HOST_NAME);
                    jettyServer.addConnector(selectChannelConnector);
                    HandlerCollection handlers = new HandlerCollection();
                    ContextHandlerCollection contexts = new ContextHandlerCollection();
                    handlers.setHandlers(new org.eclipse.jetty.server.Handler[]{contexts, new RequestHandler(CacheServer.this)});

//                    LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<>(MAX_QUEUE_SIZE);
//                    ExecutorThreadPool pool = new ExecutorThreadPool(minThreads, maxThreads, maxIdleTime, TimeUnit.MILLISECONDS, queue);
//                    ExecutorThreadPool pool = new ExecutorThreadPool(1000);
//                    ExecutorService pool = Executors.newCachedThreadPool();
//                    jettyServer.setThreadPool(pool);

                    jettyServer.addLifeCycleListener(new ServerLifecycleListener());
                    jettyServer.setHandler(handlers);
                    jettyServer.start();
                    setState(State.Started);
                } catch (Exception e) {
                    stop();
                }
            }
        });
    }

    public void stop() {
        final Server server = jettyServer;
        jettyServer = null;
        if (null != server && (server.isStarting() || server.isStarted() || server.isRunning())) {
            setState(State.Stopping);
            try {
                server.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            setState(State.Stopped);
        }
    }

    public int connections() {
        return connections.get();
    }

    private class ServerLifecycleListener implements LifeCycle.Listener {

        @Override
        public void lifeCycleStarting(LifeCycle event) {
            Logger.d("lifeCycleStarting");
            setState(State.Starting);
        }

        @Override
        public void lifeCycleStarted(LifeCycle event) {
            Logger.d("lifeCycleStarted");
            setState(State.Started);
        }

        @Override
        public void lifeCycleFailure(LifeCycle event, Throwable cause) {
            Logger.d("lifeCycleFailure");
            setState(State.Stopped);
        }

        @Override
        public void lifeCycleStopping(LifeCycle event) {
            Logger.d("lifeCycleStopping");
            setState(State.Stopping);
        }

        @Override
        public void lifeCycleStopped(LifeCycle event) {
            Logger.d("lifeCycleStopped");
            setState(State.Stopped);
        }
    }

    private static class RequestHandler extends DefaultHandler {
        private final CacheServer server;
        private static int count = 0;

        public RequestHandler(CacheServer cacheServer) {
            this.server = cacheServer;
        }

        @Override
        public void handle(String target, org.eclipse.jetty.server.Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
            count++;
            Logger.d("link count=" + count);
            handler.removeCallbacks(pendingStopServer);
            server.connections.incrementAndGet();
            LocalRequest localRequest = new LocalRequest(request);
            if (PING_URL.equals(localRequest.getUrl())) {
                response.setStatus(PING_RESPONSE_CODE);
                response.getOutputStream().close();
                return;
            }
            Source source = null;
            try {
                try {
                    source = new Source(localRequest);
                    source.open();
                    addResponseHeaders(response, source.headers());
                } catch (Exception e) {
                    responseFromRemote(localRequest, response);
                    return;
                }
                int count;
                byte[] buffer = new byte[1024];
                while ((count = source.read(buffer)) > 0) {
                    response.getOutputStream().write(buffer, 0, count);
                }
                response.getOutputStream().close();
            } catch (Exception e) {
                throw new ServletException(e);
            } finally {
                if (null != source) {
                    source.close();
                }
                int connections = server.connections.decrementAndGet();
                if (0 == connections) {
                    handler.removeCallbacks(pendingStopServer);
                    handler.postDelayed(pendingStopServer, 5000);
                }
            }
            count--;
            Logger.d("link count=" + count);
        }

        private void responseFromRemote(LocalRequest request, HttpServletResponse response) throws ServletException {
            try {
                HttpURLConnection connection = Util.createConnection(request);
                Set<String> headerNames = connection.getHeaderFields().keySet();
                for (String headerName : headerNames) {
                    if (null != headerName) {
                        response.addHeader(headerName, connection.getHeaderField(headerName));
                    }
                }
                response.setStatus(connection.getResponseCode());
                int count;
                byte[] buffer = new byte[1024];
                while ((count = connection.getInputStream().read(buffer)) > 0) {
                    response.getOutputStream().write(buffer, 0, count);
                }
                response.getOutputStream().close();
            } catch (Exception e) {
                throw new ServletException(e);
            }
        }

        private final Runnable pendingStopServer = new Runnable() {
            @Override
            public void run() {
                server.stop();
            }
        };

        private void addResponseHeaders(HttpServletResponse response, Map<String, String> headers) {
            if (null != headers) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    String key = entry.getKey();
                    if (null != key) {
                        response.addHeader(key, entry.getValue());
                    }
                }
            }
        }
    }

    /**
     * ***********************************************************************************************************
     */

    private void setStateCallback(StateCallback callback) {
        this.stateCallback = callback;
    }

    public void getUrl(String originUrl, Callback callback) {
        if (State.Started != getState()) {
            setStateCallback(new StateCallback() {

                @Override
                public void onStateChanged(State last, State current) {
                    if (State.Started.equals(current) || State.Stopped.equals(current)) {
                        setStateCallback(null);
                        feedbackUrl(current, originUrl, callback);
                    }
                }
            });
            start();
        } else {
            feedbackUrl(State.Started, originUrl, callback);
        }
    }

    private void feedbackUrl(State state, String originUrl, Callback callback) {
        if (!State.Started.equals(state)) {
            callback.onUrlGet(originUrl);
            return;
        }
        if (Looper.myLooper() == Looper.getMainLooper()) {
            try {
                callback.onUrlGet(makeUrl(originUrl));
            } catch (UnsupportedEncodingException e) {
                callback.onError(e);
            }
        } else {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        callback.onUrlGet(makeUrl(originUrl));
                    } catch (UnsupportedEncodingException e) {
                        callback.onError(e);
                    }
                }
            });
        }
    }

    private String makeUrl(String originUrl) throws UnsupportedEncodingException {
        return String.format(Locale.getDefault(), "http://%s:%d/%s", HOST_NAME, localPort, URLEncoder.encode(originUrl, "UTF-8"));
    }

    public enum State {
        Starting,
        Started,
        Stopping,
        Stopped
    }

    public interface StateCallback {
        void onStateChanged(State last, State current);
    }

    public interface Callback {
        void onUrlGet(String url);

        void onError(Throwable e);
    }
}
