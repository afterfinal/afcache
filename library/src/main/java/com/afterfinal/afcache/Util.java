package com.afterfinal.afcache;

import android.net.Uri;
import android.text.TextUtils;

import com.afterfinal.afcache.db.Column;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class Util {
    private static final char[] hexCode = "0123456789ABCDEF".toCharArray();

    public static String md5(String input) throws NoSuchAlgorithmException {
        return printHexBinary(MessageDigest.getInstance("MD5").digest(input.getBytes()));
    }

    public static String printHexBinary(byte[] data) {
        StringBuilder r = new StringBuilder(data.length * 2);
        for (byte b : data) {
            r.append(hexCode[(b >> 4) & 0xF]);
            r.append(hexCode[(b & 0xF)]);
        }
        return r.toString();
    }

    public static long parseLong(String str) {
        if (TextUtils.isEmpty(str) || !str.matches("\\d+")) {
            return 0;
        }
        return Long.parseLong(str);
    }

    public static void ensureFileExists(File file) throws Exception {
        if (file.exists()) {
            return;
        }
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        file.createNewFile();
    }

    static List<String> parseStreamToLines(LocalRequest request, InputStream is) throws Exception {
        List<String> lines = new ArrayList<>();
        String line;
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        Pattern allowCachePattern = Pattern.compile("#EXT-X-ALLOW-CACHE:(.+)", Pattern.CASE_INSENSITIVE);
        Pattern keyPattern = Pattern.compile("#EXT-X-KEY:METHOD=\"?([^\"]+)\"?,URI=\"?([^\"]+)\"?.+", Pattern.CASE_INSENSITIVE);
        while ((line = reader.readLine()) != null) {
            line = line.trim();
            Matcher allowCacheMatcher = allowCachePattern.matcher(line);
            if (allowCacheMatcher.matches()) {
                line = "#EXT-X-ALLOW-CACHE:NO";
            } else {
                Matcher keyMatcher = keyPattern.matcher(line.trim());
                if (keyMatcher.matches()) {
                    String method = keyMatcher.group(1);
                    String originUri = keyMatcher.group(2);
                    String uri = originUri;
                    if (null != AFCache.getKeyUriDecryptor()) {
                        uri = AFCache.getKeyUriDecryptor().decrypt(method, uri);
                    }
                    uri = String.format(Locale.getDefault(), "%s/%s", Consts.AFCacheSchema, toAbsUri(request.getUrl(), uri));
                    line = line.replace(originUri, uri);
                }
            }
            lines.add(line.trim());
            if (line.trim().toLowerCase().matches("#extinf:.+") || line.trim().toLowerCase().matches("#ext-x-stream-inf:.+")) {
                line = reader.readLine();
                if (null != line) {
                    lines.add(String.format(Locale.getDefault(), "%s/%s", Consts.AFCacheSchema, toAbsUri(request.getUrl(), line.trim())));
                } else {
                    break;
                }
            }
        }
        reader.close();
        return lines;
    }

    public static String toAbsUri(String context, String uri) throws Exception {
        if (uri.toLowerCase().matches("https?://.+")) {
            return uri;
        }
        return new URL(new URL(context), URLDecoder.decode(uri, "UTF-8")).toString();
    }

    public static HttpURLConnection createHeadConnection(LocalRequest request) throws Exception {
        Map<String, String> extraHeaders = new HashMap<>();
        extraHeaders.put(Consts.KEY_RANGE, "bytes=0-0");
        return createConnection("HEAD", request, extraHeaders);
    }

    public static HttpURLConnection createConnection(LocalRequest request) throws Exception {
        return createConnection(request, null);
    }

    public static HttpURLConnection createConnection(LocalRequest request, Map<String, String> extraHeaders) throws Exception {
        return createConnection(request.getMethod(), request, extraHeaders);
    }

    public static HttpURLConnection createConnection(String method, LocalRequest request, Map<String, String> extraHeaders) throws Exception {
        Map<String, String> headers = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        headers.putAll(request.getHeaders());
        if (null != extraHeaders) {
            headers.putAll(extraHeaders);
        }
        return createConnection(method, request.getUrl(), headers);
    }

    public static HttpURLConnection createConnection(String url) throws Exception {
        return createConnection("GET", url);
    }

    public static HttpURLConnection createConnection(String method, String url) throws Exception {
        Map<String, String> headers = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        return createConnection(method, url, headers);
    }

    public static HttpURLConnection createConnection(String method, String url, Map<String, String> requestHeaders) throws Exception {
        HttpURLConnection result = null;
        while (true) {
            if (url.toLowerCase().startsWith("https://")) {
                trustAllHosts();
            }
            URL u = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) u.openConnection();
            connection.setRequestMethod(method);

            requestHeaders.remove("host");
            requestHeaders.remove("remote-addr");
            requestHeaders.remove("http-client-ip");
            for (Map.Entry<String, String> entry : requestHeaders.entrySet()) {
                connection.setRequestProperty(entry.getKey(), entry.getValue());
            }

            connection.setRequestProperty("Host", u.getHost());
            if (connection instanceof HttpsURLConnection) {
                HttpsURLConnection https = (HttpsURLConnection) connection;
                https.setHostnameVerifier(DO_NOT_VERIFY);
            }
            connection.connect();

            result = connection;
            try {
                if (Status.SC_OK.getCode() == connection.getResponseCode() || Status.SC_PARTIAL_CONTENT.getCode() == connection.getResponseCode()) {
                    break;
                }
            } catch (Exception e) {
                throw e;
            }
            if (String.valueOf(connection.getResponseCode()).startsWith("30")) {
                url = connection.getHeaderField("Location");
                if (TextUtils.isEmpty(url)) {
                    url = connection.getHeaderField("location");
                }
                if (TextUtils.isEmpty(url)) {
                    break;
                }
            } else {
                break;
            }
        }
        return result;
    }

    public static Map<String, String> parseResponseHeaders(HttpURLConnection connection) {
        Map<String, String> map = new HashMap<>();
        for (Map.Entry<String, List<String>> entry : connection.getHeaderFields().entrySet()) {
            map.put(entry.getKey(), connection.getHeaderField(entry.getKey()));
        }
        return map;
    }

    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        // Android use X509 cert
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }

            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }
        }};

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public static void deleteFile(File file) {
        try {
            if (null == file || !file.exists()) {
                return;
            }
            if (file.isDirectory()) {
                File[] subFiles = file.listFiles();
                if (null != subFiles) {
                    for (File subFile : subFiles) {
                        deleteFile(subFile);
                    }
                }
            }
            file.delete();
        } catch (Exception e) {
            //TODO nothing
        }
    }

    public static String getCacheFilePath(String url) throws NoSuchAlgorithmException {
        Uri uri = Uri.parse(url);
        String host = md5(uri.getHost());
        String path = AFCache.getCacheDir().getAbsolutePath() + "/" + host + "/" + uri.getPath();
        return path.replaceAll("/{2,}", "/");
    }

    public static List<String> parseUrlsFromM3u8(String context, InputStream is) throws Exception {
        List<String> urls = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String line;
        Pattern keyPattern = Pattern.compile("#EXT-X-KEY:METHOD=\"?([^\"]+)\"?,URI=\"?([^\"]+)\"?.+", Pattern.CASE_INSENSITIVE);
        while ((line = reader.readLine()) != null) {
            line = line.trim();
            if (line.toLowerCase().startsWith("#extinf:")) {
                line = reader.readLine();
                urls.add(toAbsUri(context, line));
            } else if (line.toLowerCase().startsWith("#ext-x-key:")) {
                Matcher matcher = keyPattern.matcher(line);
                if (matcher.matches()) {
                    urls.add(toAbsUri(context, matcher.group(2)));
                }
            }
        }
        return urls;
    }

    public static boolean isConnect() {
        int count = 0;
        boolean connect = false;
        while (count < 3) {
            Runtime runtime = Runtime.getRuntime();
            Process process = null;
            try {
                process = runtime.exec("ping " + "www.baidu.com");
                InputStream is = process.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line = null;
                StringBuilder sb = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                isr.close();
                br.close();

                if (null != sb && !sb.toString().equals("")) {
                    if (sb.toString().indexOf("TTL") > 0) {
                        // 网络畅通
                        connect = true;
                    } else {
                        // 网络不畅通
                        connect = false;
                    }
                }
            } catch (Exception e) {
                connect = false;
            } finally {
                try {
                    if (null != process) {
                        process.destroy();
                    }
                } catch (Exception e) {
                    //TODO nothing
                }
            }
            if (connect) {
                break;
            }
            count++;
        }
        return connect;
    }

    public static String parseUrl(final String path) throws IOException {
        String url = path.substring(1);
        if (url.startsWith(Consts.AFCacheSchema)) {
            url = url.substring(url.indexOf("/") + 1);
        }
        url = URLDecoder.decode(url, "UTF-8");
        return url;
    }

    public static HttpURLConnection createConnection(Cache cache) throws Exception {
        JSONObject obj = new JSONObject(cache.getLocalRequest());
        String method = null;
        Map<String, String> headers = new HashMap<>();
        Iterator<String> iterator = obj.keys();
        while (iterator.hasNext()) {
            String key = iterator.next();
            headers.put(key, obj.getString(key));
            if ("method".equalsIgnoreCase(key)) {
//                dsfs
            }
        }
        return createConnection(obj.getString(Consts.KEY_METHOD), cache.getUrl(), headers);
    }

    public static List<Field> getColumns(Class<Cache> aClass) {
        Field[] fields = aClass.getDeclaredFields();
        List<Field> list = new ArrayList<>();
        for (Field field : fields) {
            Column column = field.getAnnotation(Column.class);
            if (0 == (field.getModifiers() & Modifier.STATIC) && null != column) {
                list.add(field);
            }
        }
        Collections.sort(list, new Comparator<Field>() {
            @Override
            public int compare(Field o1, Field o2) {
                Column column1 = o1.getAnnotation(Column.class);
                Column column2 = o2.getAnnotation(Column.class);
                if (null == column1 || null == column2) {
                    return 0;
                }
                return Integer.compare(column1.order(), column2.order());
            }
        });
        return list;
    }
}
