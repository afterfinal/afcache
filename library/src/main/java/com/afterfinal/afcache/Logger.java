package com.afterfinal.afcache;

import android.util.Log;

public class Logger {
    private static final String TAG = "AFCache";

    public static void d(String msg) {
        Log.d(TAG, msg);
        System.out.println(TAG + "==【" + Thread.currentThread() + "】" + msg);
    }

    public static void e(String msg) {
        Log.e(TAG, msg);
        System.err.println(TAG + "==【" + Thread.currentThread() + "】" + msg);
    }
}
