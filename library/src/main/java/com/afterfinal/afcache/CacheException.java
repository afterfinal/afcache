package com.afterfinal.afcache;

public class CacheException extends Exception {
    private final Status status;

    public CacheException(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }
}
