package com.afterfinal.afcache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CombinedCache {
    private final List<Cache> cacheList = new ArrayList<>();

    public CombinedCache(String... urls) throws Exception {
        for (String url : urls) {
            cacheList.add(Cache.get(url));
        }
    }

    public CombinedCache(Collection<String> urls) throws Exception {
        for (String url : urls) {
            cacheList.add(Cache.get(url));
        }
    }

    public void start() {
        for (Cache cache : cacheList) {
            cache.start();
        }
    }

    public void stop() {
        for (Cache cache : cacheList) {
            cache.stop();
        }
    }

    public long getCurrent() {
        long curr = 0;
        for (Cache cache : cacheList) {
            curr += cache.getCurrent();
        }
        return curr;
    }

    public long getTotal() {
        long total = 0;
        for (Cache cache : cacheList) {
            total += cache.getCurrent();
        }
        return total;
    }

    public void delete() {
        for (Cache cache : cacheList) {
            cache.delete();
        }
    }
}
