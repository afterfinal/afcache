package com.afterfinal.afcache;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public abstract class Headers {
    private static Headers requestHeaders;
    private static Headers responseHeaders;

    public static Headers request() {
        if (null == requestHeaders) {
            synchronized (SupportedRequestHeaders.class) {
                if (null == requestHeaders) {
                    requestHeaders = new SupportedRequestHeaders();
                }
            }
        }
        return requestHeaders;
    }

    public static Headers response() {
        if (null == responseHeaders) {
            synchronized (SupportedRequestHeaders.class) {
                if (null == responseHeaders) {
                    responseHeaders = new SupportedResponseHeaders();
                }
            }
        }
        return responseHeaders;
    }

    public abstract boolean support(String name);

    private static final class SupportedRequestHeaders extends Headers {

        private static final Set<String> notSupported = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);

        static {
            Collections.addAll(
                    notSupported
                    , Consts.KEY_HOST
                    , Consts.KEY_REMOTE_ADDR
                    , Consts.KEY_HTTP_CLIENT_IP
            );
        }

        @Override
        public boolean support(String name) {
            return null != name && !notSupported.contains(name);
        }
    }

    private static final class SupportedResponseHeaders extends Headers {

        private static final Set<String> notSupported = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);

        static {
            Collections.addAll(
                    notSupported
                    , Consts.KEY_CONTENT_RANGE
            );
        }

        @Override
        public boolean support(String name) {
            return null != name && !notSupported.contains(name);
        }
    }
}
