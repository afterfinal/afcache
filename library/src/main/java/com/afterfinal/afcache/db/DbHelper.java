package com.afterfinal.afcache.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.afterfinal.afcache.AFCache;
import com.afterfinal.afcache.Cache;
import com.afterfinal.afcache.FileLock;
import com.afterfinal.afcache.db.updates.Update;

@SuppressWarnings("all")
public class DbHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "AFCache";
    private static final int DB_VERSION = 1;

    private static DbHelper sInstance;
    private static Context sContext;

    public static void init(Context context) {
        sContext = context.getApplicationContext();
    }

    public static DbHelper getInstance() {
        if (null == sInstance) {
            synchronized (DbHelper.class) {
                if (null == sInstance) {
                    if (null == sContext) {
                        sContext = AFCache.getContext();
                    }
                    sInstance = new DbHelper(sContext);
                }
            }
        }
        return sInstance;
    }

    private DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Cache.createTable(db);
        FileLock.createTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String packageName = getClass().getPackage().getName();
        for (int i = oldVersion + 1; i <= newVersion; i++) {
            try {
                Update update = (Update) Class.forName(packageName + ".updates.Update" + i).newInstance();
                update.onUpgrade(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static String getString(Cursor cursor, String name) {
        return cursor.getString(cursor.getColumnIndex(name));
    }

    public static int getInt(Cursor cursor, String name) {
        return cursor.getInt(cursor.getColumnIndex(name));
    }

    public static long getLong(Cursor cursor, String name) {
        return cursor.getLong(cursor.getColumnIndex(name));
    }

    public static float getFloat(Cursor cursor, String name) {
        return cursor.getFloat(cursor.getColumnIndex(name));
    }

    public static double getDouble(Cursor cursor, String name) {
        return cursor.getDouble(cursor.getColumnIndex(name));
    }
}