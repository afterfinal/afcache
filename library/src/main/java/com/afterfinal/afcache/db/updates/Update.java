package com.afterfinal.afcache.db.updates;

import android.database.sqlite.SQLiteOpenHelper;

public interface Update {
    void onUpgrade(SQLiteOpenHelper helper);
}
