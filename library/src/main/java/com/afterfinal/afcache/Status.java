package com.afterfinal.afcache;

import android.text.TextUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class Status {
    public static final Status SC_CONTINUE = new Status(100, "Continue");
    public static final Status SC_WITCHING_PROTOCOLS = new Status(101, "witching Protocols");
    public static final Status SC_OK = new Status(200, "OK");
    public static final Status SC_CREATED = new Status(201, "Created");
    public static final Status SC_ACCEPTED = new Status(202, "Accepted");
    public static final Status SC_NON_AUTHORITATIVE_INFORMATION = new Status(203, "Non-Authoritative Information");
    public static final Status SC_NO_CONTENT = new Status(204, "No Content");
    public static final Status SC_RESET_CONTENT = new Status(205, "Reset Content");
    public static final Status SC_PARTIAL_CONTENT = new Status(206, "Partial Content");
    public static final Status SC_MULTIPLE_CHOICES = new Status(300, "Multiple Choices");
    public static final Status SC_MOVED_PERMANENTLY = new Status(301, "Moved Permanently");
    public static final Status SC_FOUND = new Status(302, "Found");
    public static final Status SC_SEE_OTHER = new Status(303, "See Other");
    public static final Status SC_NOT_MODIFIED = new Status(304, "Not Modified");
    public static final Status SC_USE_PROXY = new Status(305, "Use Proxy");
    public static final Status SC_TEMPORARY_REDIRECT = new Status(307, "Temporary Redirect");
    public static final Status SC_BAD_REQUEST = new Status(400, "Bad Request");
    public static final Status SC_UNAUTHORIZED = new Status(401, "Unauthorized");
    public static final Status SC_PAYMENT_REQUIRED = new Status(402, "Payment Required");
    public static final Status SC_FORBIDDEN = new Status(403, "Forbidden");
    public static final Status SC_NOT_FOUND = new Status(404, "Not Found");
    public static final Status SC_METHOD_NOT_ALLOWED = new Status(405, "Method Not Allowed");
    public static final Status SC_NOT_ACCEPTABLE = new Status(406, "Not Acceptable");
    public static final Status SC_PROXY_AUTHENTICATION_REQUIRED = new Status(407, "Proxy Authentication Required");
    public static final Status SC_REQUEST_TIMEOUT = new Status(408, "Request Timeout");
    public static final Status SC_CONFLICT = new Status(409, "Conflict");
    public static final Status SC_GONE = new Status(410, "Gone");
    public static final Status SC_LENGTH_REQUIRED = new Status(411, "Length Required");
    public static final Status SC_PRECONDITION_FAILED = new Status(412, "Precondition Failed");
    public static final Status SC_REQUEST_ENTITY_TOO_LARGE = new Status(413, "Request Entity Too Large");
    public static final Status SC_REQUEST_URI_TOO_LARGE = new Status(414, "Request-URI Too Large");
    public static final Status SC_UNSUPPORTED_MEDIA_TYPE = new Status(415, "Unsupported Media Type");
    public static final Status SC_REQUESTED_RANGE_NOT_SATISFIABLE = new Status(416, "Requested range not satisfiable");
    public static final Status SC_EXPECTATION_FAILED = new Status(417, "Expectation Failed");
    public static final Status SC_INTERNAL_SERVER_ERROR = new Status(500, "Internal Server Error");
    public static final Status SC_NOT_IMPLEMENTED = new Status(501, "Not Implemented");
    public static final Status SC_BAD_GATEWAY = new Status(502, "Bad Gateway");
    public static final Status SC_SERVICE_UNAVAILABLE = new Status(503, "Service Unavailable");
    public static final Status SC_GATEWAY_TIMEOUT = new Status(504, "Gateway Timeout");
    public static final Status SC_HTTP_VERSION_NOT_SUPPORTED = new Status(505, "HTTP Version not supported");

    private final int code;
    private String msg;

    public Status(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static Status parse(int code) {
        Field[] fields = Status.class.getDeclaredFields();
        for (Field field : fields) {
            if (field.getType() == Status.class && 0 != (field.getModifiers() & Modifier.STATIC)) {
                field.setAccessible(true);
                Status status = null;
                try {
                    status = (Status) field.get(null);
                } catch (IllegalAccessException e) {
                    //TODO nothing
                }
                if (null != status && status.getCode() == code) {
                    return status;
                }
            }
        }
        return new Status(500, "unknown error");
    }

    public static boolean isSuccess(int code) {
        return Status.SC_OK.getCode() == code || Status.SC_PARTIAL_CONTENT.getCode() == code;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public Status msg(String msg) {
        this.msg = msg;
        return this;
    }

    @Override
    public String toString() {
        return toString("HTTP/1.1");
    }

    public String toString(String protocol) {
        StringBuilder builder = new StringBuilder(protocol).append(" ").append(code);
        if (!TextUtils.isEmpty(msg)) {
            builder.append(" ").append(msg);
        }
        return builder.toString();
    }
}
