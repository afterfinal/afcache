package com.afterfinal.afcache;

import java.util.ArrayList;
import java.util.List;

public class CacheServerFactory {
    private static final List<CacheServer> servers = new ArrayList<>();

    public static void getUrl(String originUrl, CacheServer.Callback callback) {
        getServer().getUrl(originUrl, callback);
    }

    private static synchronized CacheServer getServer() {
        CacheServer result = null;
        for (CacheServer server : servers) {
            if (server.connections() < 5) {
                result = server;
            }
        }
        if (null == result) {
            result = new CacheServer();
            servers.add(result);
        }
        return result;
    }
}
