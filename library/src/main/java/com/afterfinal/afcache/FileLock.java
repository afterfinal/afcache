package com.afterfinal.afcache;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.afterfinal.afcache.db.DbHelper;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class FileLock {

    public interface Runnable {
        void run() throws Exception;
    }

    private static final String TB_NAME = FileLock.class.getSimpleName();

    private static final long TIME_OUT = 2000;

    private static final Map<String, HeartBeat> heartbeats = Collections.synchronizedMap(new HashMap<>());

    public static void createTable(SQLiteDatabase db) {
        db.execSQL("create table " + TB_NAME + "(filePath TEXT PRIMARY KEY NOT NULL UNIQUE, expireTime INTEGER)");
    }

    public static synchronized boolean withFileLocked(String filePath, boolean block, Runnable runnable) throws Exception {
        boolean success = lock(filePath, block);
        if (success) {
            try {
                runnable.run();
            } catch (Exception e) {
                throw e;
            } finally {
                unlock(filePath);
            }
        }
        return success;
    }

    private static boolean lock(String filePath, boolean block) {
        Logger.d("lock:" + filePath);
        boolean success = false;
        try {
            while (Thread.currentThread().isAlive() && !Thread.currentThread().isInterrupted()) {
                Cursor cursor = null;
                try {
                    cursor = DbHelper.getInstance().getReadableDatabase().rawQuery("select * from " + TB_NAME + " where filePath='" + filePath + "'", null);
                    if (0 == cursor.getCount()) {
                        cursor.close();
                        DbHelper.getInstance().getWritableDatabase().execSQL("insert into " + TB_NAME + "(filePath, expireTime) values('" + filePath + "'," + System.currentTimeMillis() + ")");
                        cursor = DbHelper.getInstance().getReadableDatabase().rawQuery("select * from " + TB_NAME + " where filePath='" + filePath + "'", null);
                        int count = cursor.getCount();
                        cursor.close();
                        success = 1 == count;
                    } else if (1 == cursor.getCount()) {
                        int index = cursor.getColumnIndex("expireTime");
                        if (index >= 0) {
                            cursor.moveToFirst();
                            long lastTime = cursor.getLong(index);
                            if (System.currentTimeMillis() - lastTime >= TIME_OUT) {
                                DbHelper.getInstance().getWritableDatabase().execSQL("delete from " + TB_NAME + " where filePath='" + filePath + "'");
                                continue;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (null != cursor && !cursor.isClosed()) {
                        cursor.close();
                    }
                }
                if (success) {
                    startHeartBeat(filePath);
                    Logger.d("lock success:" + filePath);
                    break;
                }
                if (!block) {
                    break;
                }
                try {
                    Thread.sleep(TIME_OUT);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

    private static synchronized boolean unlock(String filePath) {
        try {
            Logger.d("unlock :" + filePath);
            DbHelper.getInstance().getWritableDatabase().execSQL("delete from " + TB_NAME + " where filePath='" + filePath + "'");
            stopHeartBeat(filePath);
            Logger.d("unlock success:" + filePath);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private static boolean updateExpireTime(String filePath) {
        try {
            DbHelper.getInstance().getWritableDatabase().execSQL("update " + TB_NAME + " set expireTime=" + System.currentTimeMillis() + " where filePath='" + filePath + "'");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private static synchronized void startHeartBeat(String filePath) {
        stopHeartBeat(filePath);
        HeartBeat heartBeat = new HeartBeat(filePath);
        heartbeats.put(filePath, heartBeat);
        heartBeat.start();
    }

    private static synchronized void stopHeartBeat(String filePath) {
        HeartBeat heartBeat = heartbeats.remove(filePath);
        if (null != heartBeat) {
            heartBeat.stop();
        }
    }

    private static class HeartBeat {
        private final String filePath;

        private Timer timer;
        private TimerTask timerTask;

        public HeartBeat(String filePath) {
            this.filePath = filePath;
        }

        void start() {
            timer = new Timer();
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    updateExpireTime(filePath);
//                    Logger.d("Renew expireTime Success:" + System.currentTimeMillis());
                }
            };
            timer.schedule(timerTask, TIME_OUT / 2, TIME_OUT / 2);
        }

        void stop() {
            if (null != timerTask) {
                timerTask.cancel();
                timerTask = null;
            }
            if (null != timer) {
                timer.cancel();
                timer = null;
            }
        }
    }

    public static void main(String... args) throws Exception {
        String filePath = "";
        FileLock.withFileLocked(filePath, true, new Runnable() {
            @Override
            public void run() throws Exception {
                //TODO some thing
            }
        });
    }
}
