package com.afterfinal.afcache;

import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class DownloadManager {
    private static final int MAX_TASK_SIZE = 10;
    private static final AtomicBoolean isStart = new AtomicBoolean();
    private static final ExecutorService sExecutor = Executors.newCachedThreadPool();
    private static final ArrayBlockingQueue<Cache> taskQueue = new ArrayBlockingQueue<>(MAX_TASK_SIZE);

    public static void enqueue(Cache cache) {
        if (cache.getMyCurrent() >= cache.getMyTotal()) {
            return;
        }
        sExecutor.execute(new Runnable() {
            @Override
            public void run() {
                taskQueue.add(cache);
            }
        });
        if (isStart.compareAndSet(false, true)) {
            sExecutor.execute(new Downloader());
        }
    }

    private static class Downloader implements Runnable {

        @Override
        public void run() {
            Cache cache;
            while ((cache = Cache.nextUnfinishedCache()) != null) {
                try {
                    Cache finalCache = cache;
                    FileLock.withFileLocked(cache.getFilePath(), true, new FileLock.Runnable() {
                        @Override
                        public void run() throws Exception {
                            LocalRequest request = GsonUtil.fromJson(finalCache.getLocalRequest(), LocalRequest.class);
                            HttpURLConnection connection = Util.createConnection(request.getMethod(), finalCache.getUrl(), request.getHeaders());
                            CacheHeaders headers = new CacheHeaders();
                            headers.parse(connection);
                            finalCache.setResponseHeaders(headers.toString());
                            finalCache.save();
                            int count;
                            byte[] buffer = new byte[1024];
                            FileOutputStream fos = new FileOutputStream(finalCache.getFilePath(), true);
                            while ((count = connection.getInputStream().read(buffer)) > 0) {
                                fos.write(buffer, 0, count);
                                fos.flush();
                            }
                            fos.close();
                            connection.disconnect();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            isStart.set(false);
        }
    }
}
