package com.afterfinal.afcache;

public interface CacheFilter {
    boolean needCache(String url, String contentType);
}
