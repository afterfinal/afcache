# AFCache

#### 介绍
AFCache

#### 软件架构
以本地代理的模式实现视频、文件缓存


#### 安装教程
````
dependencies {
    implementation 'com.gitee.afterfinal:afcache:1.0.1'
}
````

#### 使用说明
````
AFCache.setCacheDir(new File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), "afcahes"));//设置缓存文件夹
AFCache.setCacheFilter();//决定哪些文件类型需要缓存
AFCache.setKeyUriDecryptor();//对于加密m3u8，自定义uri的处理
AFCache cache = new AFCache();
cache.getUrl(originUrl, new AFCache.Callback() {
	@Override
	public void onUrlGet(String url) {
		if (videoView.isPlaying()) {
			videoView.stopPlayback();
		}
		Map<String, String> headers = new HashMap<>();
		if (null != tag) {
			headers.put("Clear-Cache", tag);//tag=1，清除当前url对应的缓存
		}
		videoView.setVideoURI(Uri.parse(url), headers);
		videoView.setMediaController(new MediaController(MainActivity.this));
		videoView.start();
	}

	@Override
	public void onError(Throwable e) {
		Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
	}
});
cache.stop();
````
#### 参与贡献



#### 特技

