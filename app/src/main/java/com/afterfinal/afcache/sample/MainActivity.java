package com.afterfinal.afcache.sample;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

import com.afterfinal.afcache.AFCache;

import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    //    private static final String mp4 = "https://aliyuncdnsybstatic.chinaedu.com/resource/2020/07/21/32d7476f12684a88bed939a477586711.mp4";
    private static final String mp4 = "http://172.16.95.123:8080/video/big_buck_bunny.mp4";
    private static final String m3u8 = "https://aliyuncdn.chinaedu.com/xueshengban_hls/dc3c2c89-1fcb-4125-aa26-893a5d76d780/dc3c2c89-1fcb-4125-aa26-893a5d76d780_hq.m3u8";
    private static final String encyptedm3u8 = "http://teststatic.chinaedu.com/sfs/res1/lms3_test/resource/2020/20201126/m3u8/eb9b47ad-9080-4dbf-a230-57a10b79041a/SQ/eb9b47ad-9080-4dbf-a230-57a10b79041a_SQ.m3u8";

    private VideoView videoView;
    private AFCache cache;

    private EditText videoUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (null != getActionBar()) {
            getActionBar().hide();
        }
        if (null != getSupportActionBar()) {
            getSupportActionBar().hide();
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        AFCache.setCacheDir(new File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), "afcahes"));
//        AFCache.setCacheFilter();
//        AFCache.setKeyUriDecryptor();
        cache = new AFCache();
//        cache.stop();
        setContentView(R.layout.activity_main);
        videoUrl = findViewById(R.id.video_url);
    }

    public void playMp4(View view) {
        playVideo(mp4, String.valueOf(view.getTag()));
    }

    public void playM3u8(View view) {
        playVideo(m3u8, String.valueOf(view.getTag()));
    }

    public void playEncyptedM3u8(View view) {
        playVideo(encyptedm3u8, String.valueOf(view.getTag()));
    }

    public void playVideo(View view) {
        String url = ((EditText) findViewById(R.id.video_url)).getText().toString();
        playVideo(url, String.valueOf(view.getTag()));
    }

    private void playVideo(String originUrl, String tag) {
        CheckBox checkBox = findViewById(R.id.use_cache);
        if (!checkBox.isChecked()) {
            newVideoView(originUrl, tag);
            return;
        }
//        try {
//            Cache cache = Cache.get(originUrl);
//            cache.start();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        cache.getUrl(originUrl, new AFCache.Callback() {
            @Override
            public void onUrlGet(String url) {
                newVideoView(url, tag);
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void newVideoView(String url, String tag) {
        videoUrl.setText(url);
        if (null != videoView && videoView.isPlaying()) {
            videoView.stopPlayback();
        }
        videoView = new VideoView(this);
        FrameLayout layout = findViewById(R.id.video_view_container);
        layout.removeAllViews();
        layout.addView(videoView, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        Map<String, String> headers = new HashMap<>();
        if (null != tag) {
            headers.put("Clear-Cache", tag);
        }
        videoView.setVideoURI(Uri.parse(url), headers);
        videoView.setMediaController(new MediaController(MainActivity.this));
        videoView.start();
    }

    public void addThread(View view) {
        cache.getUrl(mp4, new AFCache.Callback() {
            @Override
            public void onUrlGet(String url) {
                new DownloadThread(url).start();
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private class DownloadThread extends Thread {
        private final String url;

        public DownloadThread(String url) {
            this.url = url;
        }

        @Override
        public void run() {
            try {
                HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
                conn.connect();
                byte[] buffer = new byte[1024];
                int count;
                while ((count = conn.getInputStream().read(buffer)) > 0) {
                    ;
                }
                boolean is = true;
                while (is) ;
                conn.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}